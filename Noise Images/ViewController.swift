//
//  ViewController.swift
//  Noise Images
//
//  Created by Nazar Lysak on 28.03.2024.
//

import UIKit

// MARK: - ViewController
class ViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var noiseImageView: UIImageView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - Private methods
private extension ViewController {
    
    func addGaussianNoise(to image: UIImage, standardDeviation: CGFloat) -> UIImage? {
        guard let cgImage = image.cgImage else { return nil }
        
        let ciImage = CIImage(cgImage: cgImage)
        
        // Генеруємо фільтр гаусівського шуму
        let noiseFilter = CIFilter(name: "CIGaussianBlur")
        noiseFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        noiseFilter?.setValue(standardDeviation, forKey: kCIInputRadiusKey)
        
        // Отримуємо вихідне зображення
        guard let outputCIImage = noiseFilter?.outputImage else { return nil }
        
        // Створюємо контекст
        let context = CIContext(options: nil)
        
        // Конвертуємо CIImage у CGImage
        guard let cgImageResult = context.createCGImage(outputCIImage, from: outputCIImage.extent) else { return nil }
        
        // Повертаємо зображення
        return UIImage(cgImage: cgImageResult)
    }
}

// MARK: - Actions
private extension ViewController {
    
    @IBAction func onSelectImageButton(_ sender: Any) {
        
        let builder = ImagePickerActionSheetBuilder()
            .addCameraAction(viewController: self)
            .addPhotosActionTitle(viewController: self)
            .addCancelAction()
        
        builder.delegate = self
        builder.present(from: self)
    }
}

// MARK: - ImagePickerActionSheetBuilderDelegate
extension ViewController: ImagePickerActionSheetBuilderDelegate {
    
    func imagePickerControllerDidCancel() {
        dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = pickedImage
            
            let noisyImage = addGaussianNoise(to: pickedImage, standardDeviation: 50)
            noiseImageView.image = noisyImage
        }
        
        picker.dismiss(animated: true)
    }
}
